import firebase from 'firebase';

const config = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: "my-react-chat.firebaseapp.com",
  databaseURL: "https://my-react-chat.firebaseio.com",
  storageBucket: "",
  messagingSenderId: process.env.REACT_APP_FIREBASE_API_ID
};
firebase.initializeApp(config);
const database = firebase.database();

export default database;
