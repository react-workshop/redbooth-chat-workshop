import React from 'react';

const Channel = ({name, isSelected, onClick}) => {
  const class_name = isSelected ? "ChannelList-item ChannelList-item-selected": "ChannelList-item";
  return <div onClick={onClick} className={class_name}>{name}</div>
};

const ChannelList = ({channels, selectedChannelId, onChannelSelect}) => (
  <div className="ChannelList">
    {
      channels.map(({id, name}) => {
        const onThisChannelSelect = () => onChannelSelect(id);
        const is_selected = selectedChannelId === id;
        return <Channel key={id} name={name} isSelected={is_selected} onClick={onThisChannelSelect} />
      })
    }
  </div>
);

export default ChannelList;